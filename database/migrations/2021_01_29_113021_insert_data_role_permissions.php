<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class InsertDataRolePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('model_has_roles', function (Blueprint $table) {
            $table->foreign('model_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
        DB::table('roles')->insert([
            [
                'name' => 'admin',
                'guard_name' => 'web',
            ],
            [
                'name' => 'guest',
                'guard_name' => 'web',
            ],
        ]);

        $user = new User();
        $user->full_name = 'admin';
        $user->user_email = 'admin@gmail.com';
        $user->password = bcrypt("admin123");
        $user->save();
        $user->assignRole([1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
