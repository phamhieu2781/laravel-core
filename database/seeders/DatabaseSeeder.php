<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use  App\Models\User;
use  App\Models\Post;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        \Illuminate\Support\Collection::times(100, function ($number) {
            User::factory()->create();
            Post::factory()->create([
                "post_code" => "P".$number.time(),
                "created_by" => $number + 1]);
        });
        DB::commit();

    }
}
