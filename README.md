## Guideline run App

Requirement: install PHP >=7.3 (https://www.php.net/downloads.php) and composer (https://getcomposer.org/download), NodeJS 14 (https://nodejs.org/en/download/) and yarn (https://yarnpkg.com/getting-started/install). After that you can execute the following commands one by one

### `cp .env.example .env`
### Modified variables in file .env
### `composer install`
### `yarn run production`
### if u want to have data test u can run this or skip it
### `php artisan db:seed`
### Last step:
### `php artisan serve`
### open http://localhost:8000 to test
### account admin: admin@gmail.com / admin123