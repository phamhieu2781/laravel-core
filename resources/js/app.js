require('./bootstrap');
    // Get the modal
$(document).ready(function (){
    $('.th-sort').on('click', function() {
        $('[name="sort_by"]').val($(this).attr('sort-by'));
        $('[name="sort_direct"]').val($(this).attr('sort-direct'));
        $('#form-search').submit();
    });
    $('button[type="submit"]').submit(function (e) {
        $(this).attr("disabled", true);
        return true;
    });
})