<div class="card">
    <div class="card-body">
        <h4 class="mb-4">{{$post ? "Update post" : "Create post"}}</h4>
        <form action="{{ $post ? route('do-update-post', $post->id) : route('do-create-post')}}" method="POST"
              enctype="multipart/form-data" id="form-parent">
            @csrf
            @if($post)
                @method('PUT')
            @else
                @method('POST')
            @endif
            <?php if(!$user->hasRole("admin")): ?>
            <div class="form-group">
                <label for="post_name" class="form-label">Post name</label>
                <input name="post_name" type="text" class="form-control w-100" id="post_name"
                       value="{{$post ? $post->post_name : ''}}">
            </div>
            <div class="form-group">
                <label for="post_description" class="form-label">Post description</label>
                <textarea name="post_description" type="text" class="form-control w-100 summernote" id="post_description"
                          rows="5">{{$post ? $post->post_description : ''}}</textarea>
            </div>
            <?php else: ?>
            <div class="form-group">
                <strong>Post name</strong>
                <p>{{$post ? $post->post_name : ''}}</p>
            </div>
            <div class="form-group">
                <strong>Post description</strong>
                <p>{!! $post ? $post->post_description : '' !!}</p>
            </div>
            <div class="form-group">
                <label for="publish" class="form-label">Status</label>
                <select class="form-control" name="publish">
                    <option value="1" {{$post && $post->publish ? "selected" : ""}}>Publish</option>
                    <option value="0" {{$post && $post->publish == 0 ? "selected" : ""}}>Unpublish</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Date publish</label>
                <input type="text" name="publish_at" class="datepicker form-control w-100" autocomplete="off"
                       onkeydown="return false" value="{{$post && $post->publish_at ? date("Y-m-d", strtotime($post->publish_at)) : ""}}">
            </div>
            <?php endif; ?>
            <div class="form-group text-right">
                <button class="btn btn-primary" type="submit">
                    Save
                </button>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            startDate: new Date(2021, 1, 1),
            setDate: new Date()
        })
    })
</script>
