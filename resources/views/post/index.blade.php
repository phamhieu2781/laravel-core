<form action="/post" method="GET" enctype="multipart/form-data" id="form-search">
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Search:
                <div class="input-group">
                    <input type="text" name="key_search" class="form-control"
                           placeholder="Post code or post name"
                           value="{{$query['key_search'] ? $query['key_search'] : ''}}"/>
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                Created date
                <input class="form-control" type="text" id="daterange"/>
                <input type="hidden" name="start_date" value="{{$query['start_date']}}">
                <input type="hidden" name="end_date" value="{{$query['end_date']}}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                Status
                <select class="form-control" name="status">
                    <option value="" {{is_null($query['status']) ? "selected" : ""}}>All</option>
                    <option value="1" {{$query['status'] ? "selected" : ""}}>Publish</option>
                    <option value="0" {{!is_null($query['status']) && $query['status'] == 0 ? "selected" : ""}}>
                        Unpublish
                    </option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary">Search</button>
            <input type="submit" class="btn btn-primary" name="clear" value="Clear"/>
        </div>
        <?php if($user->hasRole("guest")): ?>
        <div class="col-md-6 text-right">
            <a class="btn btn-primary" href="/post/create">Create post</a>
        </div>
        <?php endif; ?>
    </div>

</form>
<div class="table-responsive mt-4">
    <table class="table table-bordered" style="min-width: 1100px;">
        <thead>
        <tr>
            <th>Post code</th>
            <th>Post name</th>
            <th>Post description</th>
            @hasrole('admin')
            <th>Author</th>
            <th>Publish date</th>
            @endhasrole
            <th>Status</th>
            <th>Created date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $k => $item)
            <tr>
                <td>{{$item->post_code}}</td>
                <td>{{$item->post_name}}</td>
                <td>
                    <span class="max-3-rows">{!! $item->post_description !!}</span>
                </td>
                @hasrole('admin')
                <td>{{$item->author->full_name}}</td>
                <td>{{$item->publish_at ? date("Y-m-d", strtotime($item->publish_at)) : ""}}</td>
                @endhasrole
                <td>{{$item->publish || ($item->publish_at && strtotime($item->publish_at) <= time()) ? "Publish" : "Unpublish"}}</td>
                <td>{{$item->created_at}} <br> {{ strtotime($item->publish_at) <= time() }}</td>
                <td>
                    <a href="/post/update/{{$item->id}}"
                       class="wrap-icon-action mb-1">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>
                    @hasrole('guest')
                    <span class="wrap-icon-action mb-1 icon-delete" data-id="{{$item->id}}" data-toggle="modal"
                          data-target="#deleteModal">
                        <i class="fa fa-trash-o"></i>
                    </span>
                    @endhasrole
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-md-6">
        Showing from <strong>{!! $data->toArray()['from'] !!}</strong> to
        <strong>{!!$data->toArray()['to'] !!}</strong> on <strong>{{$data->toArray()['total']}}</strong> results
    </div>
    <div class="col-md-6 text-right">
        <div class="inline-block">
            {!! $data->links() !!}
        </div>
    </div>
</div>
@include("post.modalDelete")

<script>
    @if(Session::has('message'))
    toastr.success("{{ session('message') }}");
    @endif
    $(document).on('click', '.icon-delete', function () {
        $('#form-delete').attr("action", "/post/" + $(this).attr('data-id'));
    });

</script>
