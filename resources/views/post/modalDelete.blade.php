<div class="modal modal-danger fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="{{ route('do-delete-post', 'id') }}" method="post" id="form-delete">
                    @csrf
                    @method('DELETE')
                    <h5 class="text-center">Are u sure that u want to delete this post ?</h5>
                    <br>
                    <br>
                    <div class="text-center">
                        <span class="btn btn-secondary mr-4" data-dismiss="modal">Không</span>
                        <button type="submit" class="btn btn-danger" style="min-width: 68px;">Có</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>