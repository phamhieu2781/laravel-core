<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Laravel core</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{ asset('js/app.js') }}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Login page</h5>
                    <form method="POST" action="{{ route('do-login') }}" class="form-signin" id="my-form">
                        @csrf

                        @if(Session::has('errors'))
                            <div class="form-group text-center alert alert-danger">
                                <strong>{{ session('errors') }}</strong>
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="inputEmail">Email address</label>
                            <input type="email" name="user_email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword">Password</label>
                            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" id="btn-submit">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#my-form").submit(function (e) {
            $("#btn-submit").attr("disabled", true);
            return true;

        });
    });
</script>
</body>
</html>
