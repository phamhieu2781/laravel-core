<?php $current_url = Request::path(); ?>
<h5>
    <a class="inline-block text-black-50" href="{{ session("back_student_detail") ? session("back_student_detail") : "/classes/detail/$class_id" }}"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
</h5>
<nav class="mb-4">
    <div class="nav" id="nav-tab" role="tablist">
        <a class="nav-item nav-link {{str_contains($current_url, 'learning-history') ? 'active' : ''}}" href="/classes/detail/{{$class_id}}/student/{{$student_id}}/learning-history">Báo cáo buổi học</a>
        <a class="nav-item nav-link {{str_contains($current_url, 'report-lo') ? 'active' : ''}}"  href="/classes/detail/{{$class_id}}/student/{{$student_id}}/report-lo">Báo cáo làm bài</a>
        <a class="nav-item nav-link {{str_contains($current_url, 'report-test') ? 'active' : ''}}"  href="/classes/detail/{{$class_id}}/student/{{$student_id}}/report-test">Báo cáo bài kiểm tra</a>
        @if($admin->hasRole('admin') || $admin->can('list-support-history'))
            <a class="nav-item nav-link {{str_contains($current_url, 'support-history-learning') ? 'active' : ''}}" href="/classes/detail/{{$class_id}}/student/{{$student_id}}/support-history-learning">Lịch sử chăm sóc</a>
        @endif
        <a class="nav-item nav-link {{str_contains($current_url, 'profile') ? 'active' : ''}}"  href="/classes/detail/{{$class_id}}/student/{{$student_id}}/profile">Thông tin nhân thân</a>
    </div>
</nav>
@if(str_contains($current_url, 'profile'))
    <div class="row">
        <div class="col-md-6">
            @include("student/detail/studentProfile")
        </div>
        <div class="col-md-6">
            @include("student/detail/parentProfile")
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6">
            @include("student/detail/studentPackageInfo")
        </div>
    </div>
@elseif(str_contains($current_url, 'support-history-learning'))
    @if($admin->hasRole('admin') || $admin->can('list-support-history'))
        @include("support-history/index")
    @endif
@elseif(str_contains($current_url, 'report-lo'))
    @include("report/rateLO")
    @include("report/reportLO")
@elseif(str_contains($current_url, 'report-test'))
    @include("report/testList")
@else
    @include("student/detail/studentLearningHistory")
@endif
