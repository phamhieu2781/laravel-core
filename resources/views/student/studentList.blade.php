<div class="row">
    <div class="col-md-10">
        <h2 class="inline-block">Danh sách học sinh</h2>
        <span class="font-italic">(Cập nhật lần cuối vào {{date("d/m H:i", strtotime($last_updated_at))}}
            Lần tới vào:
            @if(date("H", strtotime($last_updated_at) == 12))
                {{date("d/m H:i", strtotime($last_updated_at) + 3600*5)}})
            @elseif(date("H", strtotime($last_updated_at) == 17))
                {{date("d/m H:i", strtotime($last_updated_at) + 3600*7)}})
            @else
                {{date("d/m H:i", strtotime($last_updated_at) + 3600*12)}})
            @endif
        </span>
    </div>
    <div class="col-md-2 text-md-right">
        <div class="form-group">
            <button class="btn btn-primary inline-block" id="export-list-students">Export Excel</button>
        </div>
    </div>
</div>

<form action="/student" method="GET" enctype="multipart/form-data" id="form-search">
    <input type="hidden" name="sort_by">
    <input type="hidden" name="sort_direct">
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Tìm kiếm :
                <div class="input-group">
                    <input type="text" name="key_search" class="form-control"
                           placeholder="Tên hoặc số điện thoại"
                           value="{{$query['key_search'] ? $query['key_search'] : ''}}"/>
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Trạng thái :
                <select class="form-control" name="student_status" onChange="submitForm()">
                    <option value="">Tất cả</option>
                    @foreach ($all_status as $status)
                        <option
                            value="{{$status}}" {{isset($query['student_status']) && $query['student_status'] == $status ? 'selected' : ''}}>{{$status}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Giáo viên :
                <select class="form-control" name="teacher_id" onChange="submitForm()">
                    <option value="">Tất cả</option>
                    <option value="0" {{isset($query['teacher_id']) && $query['teacher_id'] == 0 ? 'selected' : ''}}>
                        Chưa được phân
                    </option>
                    @foreach ($all_teachers as $teacher)
                        <option
                            value="{{$teacher->id}}" {{isset($query['teacher_id']) && $query['teacher_id'] == $teacher->id ? 'selected' : ''}}>{{$teacher->user_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Trình độ :
                <select class="form-control" name="class_level_id" onChange="submitForm()">
                    <option value="">Tất cả</option>
                    @foreach ($all_class_level as $class_level)
                        <option value="{{$class_level->id}}"  {{isset($query['class_level_id']) && $query['class_level_id'] == $class_level->id ? 'selected' : ''}}>{{$class_level->desc}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Lịch học :
                <select class="form-control" name="class_days" onChange="submitForm()">
                    <option value="">Tất cả</option>
                    @foreach ($all_class_days as $class_day)
                        <option
                            value="{{$class_day}}" {{isset($query['class_days']) && $query['class_days'] == $class_day ? 'selected' : ''}}>{{$class_day}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Ngày học :
                <input type="text" name="learning_date" class="datepicker form-control w-100" autocomplete="off"
                       value="{{ !empty($query['learning_date']) ? $query['learning_date'] : '' }}" onkeydown="return false">
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Tham gia học :
                <select class="form-control" name="join_status" onChange="submitForm()" {{empty($query['learning_date']) ? 'disabled' : ''}}>
                    <option value="">Tất cả</option>
                    <option value="1" {{isset($query['join_status']) && $query['join_status'] == 1 ? 'selected' : ''}}>Có</option>
                    <option value="0" {{isset($query['join_status']) && $query['join_status'] == 0 ? 'selected' : ''}}>Không</option>
                </select>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Mức độ chăm chỉ :
                <select class="form-control" name="hard_working_level_id" onChange="submitForm()" >
                    <option value="">Tất cả</option>
                    @foreach ($all_hard_working_level as $hard_working_level)
                        <option
                            value="{{$hard_working_level->id}}" {{$query['hard_working_level_id'] == $hard_working_level->id ? 'selected' : ''}}>{{$hard_working_level->hard_working_level_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Đã trả tiền :
                <select class="form-control" name="payment" onChange="submitForm()">
                    <option value="">Tất cả</option>
                    <option value="1" {{isset($query['payment']) && $query['payment'] == 1 ? 'selected' : ''}}>Có</option>
                    <option value="0" {{isset($query['payment']) && $query['payment'] == 0 ? 'selected' : ''}}>Không</option>
                </select>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Buổi học đầu tiên :
                <input type="text" name="first_learning_date" class="first-learning-date form-control w-100" autocomplete="off"
                       value="{{ !empty($query['first_learning_date']) ? $query['first_learning_date'] : '' }}" onkeydown="return false">
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Search</button>
        <input type="submit" class="btn btn-primary" name="clear" value="Clear" />
    </div>
</form>
<div class="table-responsive mt-4">
    <table class="table table-bordered"  style="min-width: 1100px;">
        <thead>
        <tr>
            <th style="width: 10%;">Thông tin học sinh</th>
            <th style="width: 10%;">Lớp học</th>
            <th style="width: 10%;">GVPT</th>
            <th style="width: 10%;">Ngày bắt đầu</th>
            <th style="width: 10%;">Ngày kết thúc</th>
            <th style="width: 20%;">Liên hệ mới nhất</th>
            <th style="width: 5%;">Tham gia học</th>
            <th style="width: 5%;">Trình độ</th>
            <th style="width: 5%;">Hành động</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $k => $item)
            <tr>
                    <td>
                        {{ $item->full_name }} <br>
                        {{ $item->user_name }} <br>
                        @if($item->current_student_sustenance)
                            <span class="custom-tag suspended small-tag">
                              Suspended
                        </span>
                        @elseif($item->student_summery && $item->student_summery->student_status)
                            <span class="custom-tag {{ $item->student_summery->student_status }} small-tag">
                              {{ $item->student_summery->student_status }}
                        </span>
                        @endif
                        {!! $item->parent && $item->parent->zalo_status ? $item->parent->zalo_status->zalo_status_name : '' !!}
                    </td>
                    <td>
                        @if( $item->student_summery && $item->student_summery->current_class)
                            {{ $item->student_summery->current_class->class_code }} <br>
                            @if($item->student_summery->current_class->class_level)
                                <span class="custom-tag small-tag {{$item->student_summery->current_class->class_level_id == 1 ? 'onboarding' : 'suspended'}}">{{$item->student_summery->current_class->class_level->desc}}</span>
                            @endif
                        @endif
                    </td>
                    <td>{{ $item->student_summery && $item->student_summery->current_class && $item->student_summery->current_class->main_teacher ? $item->student_summery->current_class->main_teacher->user_name : '' }}</td>
                    <td>
                        {{$item->student_summery && $item->student_summery->expire_at ? date('d/m/Y', strtotime($item->student_summery->start_at)) : ''}}
                    </td>
                    <td>
                        {{$item->student_summery && $item->student_summery->start_at ? date('d/m/Y', strtotime($item->student_summery->expire_at)) : ''}}
                    </td>
                    <td>
                        @if($item->last_student_support)
                            <span class="max-3-rows">
                                {{ $item->last_student_support->contacted_at ? date('d/m/Y', strtotime($item->last_student_support->contacted_at)) : date('d/m/Y', strtotime($item->last_student_support->date_off))}}:
                                {{$item->last_student_support->reason_detail}}
                            </span>
                            @if($item->last_student_support->hard_working_level)
                                <span class="custom-tag small-tag suspended">
                                    {{ $item->last_student_support->hard_working_level->hard_working_level_name}}
                                </span>
                            @endif
                        @endif
                    </td>
                    <td>
                        @if($query['learning_date'])
                            {!! $item->last_student_calender && $item->last_student_calender->join_status ? '<span class="color-green">Có</span>' : '<span class="color-red">Không</span>' !!}
                        @endif
                    </td>
                    <td>
                        @if($item->class_level)
                            <div>{{substr($item->class_level->name, 0,1)}} - {{ $item->class_level->desc}}</div>
                        @endif
                        <span class="wrap-icon-action edit-student-icon" data-toggle="modal" data-target="#modalUpdateStudent" student-id="{{$item->id}}" student-name="{{$item->full_name}}" class-level-id="{{$item->class_level_id}}">
                            <i class="fa fa-edit fa-fw"></i>
                        </span>
                    </td>
                    <td>
                        <a href="/issue/create?student_id={{ $item->id }}" class="wrap-icon-action mb-1">
                            <i class="fa fa-bug"></i>
                        </a>
                        @if($item->student_summery && $item->student_summery->current_class)
                            <a href="/classes/detail/{{$item->student_summery->class_id}}/student/{{$item->id}}/learning-history"
                               class="wrap-icon-action mb-1">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{config("constants.REPORT_DOMAIN")}}er-report/{{$item->user_name}}/{{$item->student_summery->current_class->grade_id}}/{{$item->student_summery->current_class->subject_id}}" target="_blank"
                               class="wrap-icon-action mb-1">
                                <i class="fa fa-line-chart"></i>
                            </a>
                        @else
                            <button type="button" class="wrap-icon-action disabled mb-1" disabled><i class="fa fa-eye"></i></button>
                            <button type="button" class="wrap-icon-action disabled mb-1" disabled><i class="fa fa-line-chart"></i></button>
                        @endif
                    </td>
                </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-md-6">
        Hiển thị kết quả từ <strong>{!! $data->toArray()['from'] !!}</strong> đến
        <strong>{!!$data->toArray()['to'] !!}</strong> trên <strong>{{$data->toArray()['total']}}</strong> kết quả
    </div>
    <div class="col-md-6 text-right">
        <div class="inline-block">
            {!! $data->links() !!}
        </div>
    </div>
</div>
@include("student/modalUpdateStudent")
<script>

    @if(Session::has('message'))
    toastr.success("{{ session('message') }}");
    @endif
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            startDate: new Date(2020, 6, 1),
            endDate: new Date(),
            setDate: new Date(),
            multidate: 6,
            closeOnDateSelect: false,
        }).on('hide', function(ev) {
            submitForm();
        });
        $('.first-learning-date').datepicker({
            format: 'dd/mm/yyyy',
            startDate: new Date(2020, 6, 1),
            endDate: new Date(),
            setDate: new Date(),
            closeOnDateSelect: false,
        }).on('hide', function(ev) {
            submitForm();
        });
    })

</script>
