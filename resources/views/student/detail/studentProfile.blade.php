<div class="card h-100">
    <div class="card-body">
        <h4 class="mb-4">Thông tin học sinh</h4>
        <form action="{{route('do-update-student', $student->id)}}" method="POST" enctype="multipart/form-data" id="form-student">
            @csrf
            @method('PUT')
            <div class="form-group row form-inline">
                <label for="studentName" class="col-lg-3 col-xl-2 col-form-label">Họ tên</label>
                <div class="col-lg-9 col-xl-10">
                    <input name="full_name" type="text" class="form-control w-100" id="studentName"
                           value="{{$student->full_name}}" readonly>
                </div>
            </div>
            <div class="form-group row form-inline">
                <label for="birthday" class="col-lg-3 col-xl-2 col-form-label">Ngày sinh</label>
                <div class="col-lg-9 col-xl-10">
                    <input type="text" name="birthday" class="datepicker form-control w-100" autocomplete="off" value="{{$student->birthday ? date('d/m/Y', strtotime($student->birthday)) : ''}}">
                </div>
            </div>
            <div class="form-group row form-inline">
                <label for="gender" class="col-lg-3 col-xl-2 col-form-label">Giới tính</label>
                <div class="col-lg-9 col-xl-10">
                    <select class="form-control w-100" name="gender" id="gender">
                        <option value=""></option>
                        <option value="0" {{!is_null($student->gender) && $student->gender == 0 ? 'selected' : ''}}>Nam</option>
                        <option value="1" {{$student->gender == 1 ? 'selected' : ''}}>Nữ</option>
                    </select>
                </div>
            </div>
            <div class="form-group row form-inline">
                <label for="studentSchool" class="col-lg-3 col-xl-2 col-form-label">Trường</label>
                <div class="col-lg-9 col-xl-10">
                    <input name="school" type="text" class="form-control w-100" id="studentSchool"
                           value="{{$student->school}}">
                </div>
            </div>
            <div class="form-group row form-inline">
                <label for="studentClass" class="col-lg-3 col-xl-2 col-form-label">Lớp</label>
                <div class="col-lg-9 col-xl-10">
                    <input type="text" readonly class="form-control w-100" id="studentClass"
                           value="{{$student->student_summery && $student->student_summery->current_class ? $student->student_summery->current_class->class_code : ''}}">
                </div>
            </div>
            <div class="form-group text-right">
                <button class="btn btn-primary" type="submit">
                    Cập nhật
                </button>
            </div>
        </form>
    </div>
</div>
<script>
    @if(Session::has('message'))
    toastr.success("{{ session('message') }}");
    @elseif(Session::has('errors'))
        toastr.options =
        {
            "closeButton" : true,
            "positionClass" : "toast-bottom-right",
        }
    toastr.error("{{ session('errors') }}");
    @endif
    $(document).ready(function (){
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            startDate: new Date(2013, 12, 31),
        });
    })
</script>
