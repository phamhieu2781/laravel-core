<div class="card">
    <div class="card-body">
        <h4 class="mb-4">Thông tin mua hàng</h4>
        <div class="mt-1">
            <strong>Số lần mua:</strong> {{$student->student_summery && !is_null($student->student_summery->count_extend) ? $student->student_summery->count_extend + 1 : ''}}
        </div>
        <div class="mt-1">
            <ul><strong>Các giai đoạn gia hạn:</strong></ul>
            @if($sgs)
                @foreach($sgs as $item)
                    <li>
                        Từ <strong>{{date("d/m/Y", strtotime($item->start_date))}}</strong> đến <strong>{{date("d/m/Y", strtotime($item->end_date))}}</strong>
                    </li>
                @endforeach
            @endif
        </div>
    </div>
</div>
