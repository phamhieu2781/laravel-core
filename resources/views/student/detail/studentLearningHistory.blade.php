<div class="learning-history">
    <div class="learning-history-left">
        <div class="item-learning-history font-weight-bold">
            <div class="item-learning-history-td">
                Thời gian
            </div>
            <div class="item-learning-history-td">
                Thời gian vào lớp Live
            </div>
            <div class="item-learning-history-td">
                Đánh giá lớp Live
            </div>
            <div class="item-learning-history-td">
                Thời gian học lớp coach
            </div>
            <div class="item-learning-history-td">
                Số quiz làm trong ngày
            </div>
            <div class="item-learning-history-td">
                Tỉ lệ trả lời đúng
            </div>
            <div class="item-learning-history-td">
                Mã lớp
            </div>
        </div>
    </div>
    <div class="learning-history-right">
        <div class="scroll-learning-history">
            <div class="wrap-learning-history">
                @foreach($learning_history as $k => $item)
                    <div class="item-learning-history">
                        <div class="item-learning-history-td">
                            {{preg_replace('/(\d{4})(\d{2})(\d{2})/', '$3/$2/$1', $k)}}
                        </div>
                        <div class="item-learning-history-td">
                            {{ $item && $item->time_joined_class ? $item->time_joined_class." - ".date("H:i", strtotime($item->time_joined_class) + $item->time_joined_ls) : '' }}
                        </div>
                        <div class="item-learning-history-td">
                            {{ $item && $item->rating_class > 0 ? $item->rating_class : ''}}
                        </div>
                        <div class="item-learning-history-td">
                            {{ $item ? round($item->time_joined_coaching/60)." phút" : ''}}
                        </div>
                        <div class="item-learning-history-td">
                            {{ $item ? $item->total_number_quiz : ''}}
                        </div>
                        <div class="item-learning-history-td">
                            {{ $item ? "$item->total_number_correct_quiz/$item->total_number_quiz" : ''}}
                            {{ $item && $item->total_number_quiz > 0 ? "(".round($item->total_number_correct_quiz*100/$item->total_number_quiz)."%)" : ""}}
                        </div>
                        <div class="item-learning-history-td">
                            {{ $item ? $item->class_code : '' }}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
