<div class="card">
    <div class="card-body">
        <h4 class="mb-4">Thông tin phụ huynh</h4>
        <form action="{{ $student->parent ? route('do-update-parent', $student->parent->id) : route('do-create-parent')}}" method="POST" enctype="multipart/form-data" id="form-parent">
            @csrf
            @if($student->parent)
                @method('PUT')
            @else
                @method('POST')
            @endif
            <input type="hidden" name="student_id" value="{{$student->id}}">
            <div class="form-group row form-inline">
                <label for="parentName" class="col-lg-3 col-xl-2 col-form-label">Họ tên</label>
                <div class="col-lg-9 col-xl-10">
                    <input name="parent_name" type="text" class="form-control w-100" id="parentName" value="{{$student->parent ? $student->parent->parent_name : ''}}">
                </div>
            </div>
            <div class="form-group row form-inline">
                <label for="parentPhone" class="col-lg-3 col-xl-2 col-form-label">SĐT</label>
                <div class="col-lg-9 col-xl-10">
                    <input name="parent_phone" type="tel" class="form-control w-100" id="parentPhone" pattern="^(\d){8,12}$" value="{{$student->parent ? $student->parent->parent_phone : $student->phone}}" required>
                </div>
            </div>
            <div class="form-group row form-inline">
                <label for="parentAddress" class="col-lg-3 col-xl-2 col-form-label">Địa chỉ</label>
                <div class="col-lg-9 col-xl-10">
                    <input name="parent_address" type="text" class="form-control w-100" id="parentAddress" value="{{$student->parent ? $student->parent->parent_address : ''}}">
                </div>
            </div>
            <div class="form-group row form-inline">
                <label for="parentZalo" class="col-lg-3 col-xl-2 col-form-label">Zalo</label>
                <div class="col-lg-9 col-xl-10">
                    <input name="parent_zalo" type="text" class="form-control w-100" id="parentZalo" value="{{ $student->parent ? $student->parent->parent_zalo : '' }}">
                </div>
            </div>
            <div class="form-group row form-inline">
                <label for="parentZalo" class="col-lg-3 col-xl-2 col-form-label">Kết bạn zalo</label>
                <div class="col-lg-9 col-xl-10">
                    <select class="form-control w-100" name="zalo_status_id">
                        <option value=""></option>
                        @if(isset($student->parent))
                            @foreach ($all_zalo_status as $item)
                                <option
                                    value="{{$item->id}}" {{$item->id === $student->parent->zalo_status_id ? 'selected' : ''}}>{{$item->zalo_status_name}}</option>
                            @endforeach
                        @else
                            @foreach ($all_zalo_status as $item)
                                <option
                                    value="{{$item->id}}">{{$item->zalo_status_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group text-right">
                <button class="btn btn-primary" type="submit">
                    Cập nhật
                </button>
            </div>
        </form>
    </div>
</div>
