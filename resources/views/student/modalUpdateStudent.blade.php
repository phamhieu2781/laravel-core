<div class="modal fade" id="modalUpdateStudent" tabindex="-1" role="dialog" aria-labelledby="modalUpdateStudent" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-11">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Cập nhật trình độ học sinh
                        <span id="student-name"></span>
                    </h5>
                </div>
            </div>
            <form action="/student/update/{id}" method="POST" enctype="multipart/form-data" id="form-update-student">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="form-group">
                        <select class="form-control" name="class_level_id" required>
                            <option value=""></option>
                            @foreach ($all_class_level as $class_level)
                                <option value="{{$class_level->id}}">{{substr($class_level->name, 0,1)}} - {{$class_level->desc}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <span type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</span>
                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                </div>
            </form>
        </div>
    </div>
</div>
