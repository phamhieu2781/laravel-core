<div class="container-tabs">
    <a class="tab-item active" href="/user">
        Quản lý tài khoản
    </a>
    <a class="tab-item" href="/role">
        Quản lý role
    </a>
</div>
<form action="/user" method="GET" enctype="multipart/form-data" id="form-search">
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Tìm kiếm :
                <div class="input-group">
                    <input type="text" name="key_search" class="form-control"
                           placeholder="Nhập email hoặc số điện thoại"
                           value="{{$query['key_search'] ? $query['key_search'] : ''}}"/>
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                Role :
                <select class="form-control" name="role_name" onChange="submitForm()">
                    <option value="">Tất cả</option>
                    @foreach ($all_roles as $role)
                        <option
                            value="{{$role->name}}" {{isset($query['role_name']) && $query['role_name'] == $role->name ? 'selected' : ''}}>{{$role->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6 text-right">
            @if($admin->hasRole('admin') || $admin->can('create-user'))
                <br>
                <div class="form-group">
                    <a href="/user/create" class="btn btn-primary inline-block">Tạo tài khoản</a>
                </div>
            @endif
        </div>
    </div>
</form>
<div class="table-responsive mt-4">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Tên</th>
            <th>Email</th>
            <th>Số điện thoại</th>
            <th>Role</th>
            <th style="min-width: 125px;">Hành động</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $k => $item)
            <tr>
                <td>{{ $item->user_name }}</td>
                <td>{{ $item->user_email }}</td>
                <td>{{ $item->user_phone }}</td>
                <td>
                    @if(!empty($item->getRoleNames()))
                        @foreach($item->getRoleNames() as $v)
                           {{ $v }}
                        @endforeach
                    @endif
                </td>
                <td>
                    @if(empty($item->getRoleNames()) || empty($item->getRoleNames()[0]) || $item->getRoleNames()[0] !== 'admin')
                        <a href="/user/edit/{{ $item->id }}" class="btn btn-outline-primary mb-1">Chỉnh sửa</a> &nbsp;
                        <a href="#" data-id="{{$item->id}}" class="btn btn-outline-danger delete mb-1" data-toggle="modal" data-target="#deleteModal" >Delete</a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-md-6">
        Hiển thị kết quả từ <strong>{!! $data->toArray()['from'] !!}</strong> đến <strong>{!!$data->toArray()['to'] !!}</strong> trên <strong>{{$data->toArray()['total']}}</strong> kết quả
    </div>
    <div class="col-md-6 text-right">
        <div class="inline-block">
            {!! $data->links() !!}
        </div>
    </div>
</div>
@include('user.userModalDelete')
<script>
    @if(Session::has('message'))
    toastr.success("{{ session('message') }}");
    @endif
    $(document).on('click','.delete',function(){
        let id = $(this).attr('data-id');
        $('#id').val(id);
    });
</script>
