<div class="modal modal-danger fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="{{ route('delete-user', 'id') }}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" id="id" name="id"/>
                    <h5 class="text-center">Bạn có chắc chắn muốn xóa tài khoản này ?</h5>
                    <br>
                    <br>
                    <div class="text-center">
                        <span class="btn btn-secondary mr-4" data-dismiss="modal">Không</span>
                        <button type="submit" class="btn btn-danger" style="min-width: 68px;">Có</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
