<div class="page-edit-issue">
    <form action="{{route('do-change-password') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label class="mb-3">Mật khẩu cũ</label>
            <input class="form-control" name="old_password" type="password" value=""  required>
        </div>
        <div class="form-group">
            <label class="mb-3">Mật khẩu mới</label>
            <input class="form-control" name="password" type="password" value=""  required>
        </div>
        <div class="form-group">
            <label class="mb-3">Nhập lại mật khẩu mới</label>
            <input class="form-control" name="confirm-password" type="password" value=""  required>
        </div>
        <div class="form-group text-right">
            <button class="btn btn-primary" type="submit">
                Cập nhật
            </button>
        </div>
    </form>
</div>

<script>
    @if(Session::has('errors'))
        toastr.error("{{ session('errors') }}");
    @elseif(Session::has('message'))
        toastr.success("{{ session('message') }}");
    @endif
</script>
