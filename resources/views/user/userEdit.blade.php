<div class="page-edit-issue">
    <form action="{{ isset($user) ? route('do-edit-user', ['id' => $user['id']]) : route('do-create-user') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @if(isset($user))
            @method('PUT')
        @else
            @method('POST')
        @endif
        <div class="form-group">
            <label class="mb-3">Tên</label>
            <input class="form-control" name="user_name" type="text" value="{{ isset($user) ? $user->user_name : '' }}" autocomplete="false"  required>
        </div>
        <div class="form-group">
            <label class="mb-3">Email</label>
            <input class="form-control" name="user_email" type="email" value="{{ isset($user) ? $user->user_email : '' }}" {{isset($user) ? 'readonly' : ''}} autocomplete="false" required>
        </div>
        <div class="form-group">
            <label class="mb-3">Số điện thoại</label>
            <input class="form-control" name="user_phone" type="tel" value="{{ isset($user) ? $user->user_phone : '' }}" autocomplete="false">
        </div>
        @if(!isset($user))
            <div class="form-group">
                <label class="mb-3">Mật khẩu</label>
                <input class="form-control" name="password" type="password" value="" autocomplete="false" required>
            </div>
            <div class="form-group">
                <label class="mb-3">Nhập lại mật khẩu</label>
                <input class="form-control" name="confirm-password" type="password" value="" autocomplete="false" required>
            </div>
        @endif
        <div class="form-group">
            <label class="mb-3">Role</label> <br>
            <select class="form-control" name="role_id" required>
                <option value=""></option>
                @if(isset($user))
                    @foreach ($roles as $item)
                        <option value="{{$item->id}}" {{$item->id === $user['role_id'] ? 'selected' : ''}}>{{$item->name}}</option>
                    @endforeach
                @else
                    @foreach ($roles as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group text-right">
            <button class="btn btn-primary" type="submit">
                {{ isset($user) ? 'Cập nhật' : 'Tạo' }}
            </button>
        </div>
    </form>
</div>

<script>
    @if(Session::has('errors'))
        toastr.options =
        {
            "closeButton" : true,
            "positionClass" : "toast-bottom-right",
        }
    toastr.error("{{ session('errors') }}");
    @endif
</script>
