<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Laravel core</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="{{ asset('libraries/moment/moment.min.js') }}"></script>
    <link href="{{ asset('css/app.css') }}?v={{config('constants.VERSION')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('libraries/font-awesome/css/font-awesome.min.css') }}">
    <script src="{{ asset('js/app.js') }}?v={{config('constants.VERSION')}}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('libraries/toastr/toastr.min.css') }}">
    <script src="{{ asset('libraries/toastr/toastr.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('libraries/bootstrap-select/bootstrap-select.min.css') }}">
    <script src="{{ asset('libraries/bootstrap-select/bootstrap-select.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('libraries/select2/select2.min.css') }}">
    <script src="{{ asset('libraries/select2/select2.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('libraries/daterangepicker/daterangepicker.min.css') }}">
    <script src="{{ asset('libraries/daterangepicker/daterangepicker.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('libraries/summernote/summernote-bs4.min.css') }}">
    <script src="{{ asset('libraries/summernote/summernote-bs4.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('libraries/bootstrap-datepicker/datepicker.min.css') }}">
    <script src="{{ asset('libraries/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

    <script>
        toastr.options =
            {
                "closeButton": true,
                "positionClass": "toast-bottom-right",
            }
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</head>
<div id="wrapper">
    @include('common.header')
    <div class="container mt-4">
        @include($content)
    </div>
</div>
<script src="{{ asset('js/custom/app-daterange.js') }}"></script>
<script src="{{ asset('js/custom/modal.js') }}"></script>
<script type="text/javascript">
    function submitForm() {
        $('#form-search').submit();
    }

    $('.summernote').summernote({
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['link']],
        ],
        callbacks: {
            onImageUpload: function (data) {
                data.pop();
            }
        }
    });
    $("form").on("submit", function () {
        $('button.btn[type="submit"]').attr("disabled", true);
    });
</script>
</body>
</html>
