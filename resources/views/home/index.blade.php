@foreach($data as $item)
    <div class="card mt-4">
        <div class="card-body">
            <h6><strong>{{$item->post_name}}</strong></h6>
            <p>{!! $item->post_description !!}</p>
            @if($item->author)
                <div class="row">
                    <div class="col-md-6">
                        <span><strong>Author:</strong>{{$item->author->full_name}}</span>
                    </div>
                    <div class="col-md-6 text-right">
                        <span class="inline-block">{{$item->created_at}}</span>
                    </div>
                </div>
            @endif

        </div>
    </div>
@endforeach
<div class="row mt-4">
    <div class="col-md-6">
        Showing from <strong>{!! $data->toArray()['from'] !!}</strong> to
        <strong>{!!$data->toArray()['to'] !!}</strong> on <strong>{{$data->toArray()['total']}}</strong> results
    </div>
    <div class="col-md-6 text-right">
        <div class="inline-block">
            {!! $data->links() !!}
        </div>
    </div>
</div>