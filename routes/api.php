<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::namespace('api')->group(function () {
    Route::group(['prefix'=>'/student', ],function(){
        Route::get('/', 'StudentController@get_list_students');
    });
    Route::group(['prefix'=>'/student-wishes', ],function(){
        Route::get('/{id}', 'StudentWishController@get_student_wishes_by_student_id');
        Route::post('/', 'StudentWishController@create_student_wishes');
    });

    Route::group(['prefix'=>'/student-up-grade', ],function() {
        Route::get('/{student_id}', 'StudentUpGradeController@get_student_up_grade_by_student_id');
        Route::put('/{student_id}', 'StudentUpGradeController@update_student_up_grade_by_student_id');
    });
});
