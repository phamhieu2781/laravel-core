<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/login', 'AuthController@do_login')->name('do-login');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/register', 'AuthController@do_register')->name('do-register');
Route::get('/', 'HomeController@index')->name('homepage');

Route::middleware('auth')->group(function (){
    Route::get('/logout', 'AuthController@logout')->name('logout');

    Route::group(['prefix'=>'/post'],function(){
        Route::get('/', 'PostController@index')->name("list-post");
        Route::get('/create', 'PostController@create');
        Route::post('/create', 'PostController@do_create')->name("do-create-post");
        Route::get('/update/{id}', 'PostController@update');
        Route::put('/put/{id}', 'PostController@do_update')->name("do-update-post");
        Route::delete('/{id}', 'PostController@destroy')->name("do-delete-post");
    });

    Route::fallback(function () {
        return redirect()->route('homepage');
    });
});

//Route::group(['prefix'=>'/parent'],function(){
//    Route::get('/login', 'AuthParentController@login');
//    Route::get('/report-learning', 'ParentReportController@get_student_learning_history');
//    Route::get('/report-lo', 'ParentReportController@get_student_report_lo');
//    Route::get('/report-test', 'ParentReportController@test_list_by_student_id');
//    Route::get('/profile', 'ParentReportController@get_student_profile_by_id');
//    Route::get('/report-test/{test_id}', 'ParentReportController@get_test_by_student_test_id');
//});
