$(document).ready(function (){
    $(".edit-icon").on("click", function (){
        $('[name="class_id"]').val($(this).attr("class-id"));
        $('[name="user_id"]').val($(this).attr("user-id"));
        $('#class-code').text($(this).attr("class-code"));
    });

    $(".edit-student-icon").on("click", function (){
        $("#form-update-student").attr("action", "/student/update/"+ $(this).attr("student-id"));
        $('[name="class_level_id"]').val($(this).attr("class-level-id"));
        $('#student-name').text($(this).attr("student-name"));
    });
})
