<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasFactory, HasRoles;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope);
    }

    protected $fillable = [
        'user_name',
        'user_email',
        'user_phone',
        'password',
        'active',
        'create_by',
        'updated_by',
    ];

    protected $hidden = [
        'password',
        'create_by',
        'updated_by',
    ];


}
