<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope);
    }

    public function scopeSearch($query, $key_search)
    {
        return $query->where(function ($q) use ($key_search) {
            $q->whereRaw('lower(post_code) like ?', ['%' . mb_strtolower($key_search) . '%'])
                ->orWhereRaw('lower(post_name) like ?', ['%' . mb_strtolower($key_search) . '%']);
        });
    }

    public function scopePublish($query)
    {
        return $query->where(function ($q) {
            $q->where("publish", 1)
                ->orWhere('publish_at', '<=', date("Y-m-d H:i:s"));
        });
    }

    public function author()
    {
        return $this->belongsTo(User::class, "created_by");
    }
}
