<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index(Request $request)
    {
        if(!empty($request->clear)){
            return redirect("/post");
        }
        $list = Post::with("author")->orderByDesc("id");
        $user = auth()->user();
        if (!$user->hasRole("admin")) {
            $list = $list->where("created_by", $user->id);
        }
        if ($request->key_search) {
            $list = $list->search($request->key_search);
        }
        if ($request->start_date && $request->end_date) {
//            $list = $list->whereBetween("created_at", [$request->start_date, $request->end_date]);
            $list = $list->whereDate("created_at", ">=", $request->start_date)
                ->whereDate("created_at", "<=", $request->end_date);
        }
        if ($request->status) {
            $list = $list->where("publish", $request->status);
        }
        $list = $list->paginate(10);
        $query = [
            'key_search' => $request->key_search,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'status' => $request->status,
        ];
        $data_view = [
            'content' => 'post/index',
            'data' => $list,
            'query' => $query,
            'user' => $user,
        ];
        return view('mainLayout', $data_view);
    }

    public function create()
    {
        $data_view = [
            'content' => 'post/edit',
            'post' => null
        ];
        return view('mainLayout', $data_view);
    }

    public function do_create(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'post_name' => 'required|min:0|max:255',
                'post_description' => 'required|min:0|max:65535',
            ]);
        if ($validator->fails()) {
            return redirect()->back()->with("errors", $validator->errors());
        }
        $user_id = auth()->user()->id;
        $post = new Post();
        $post->post_code = "P" . $user_id . time();
        $post->post_name = $request->post_name;
        $post->post_description = $request->post_description;
        $post->created_by = $user_id;
        $post->publish = 0;
        $post->save();
        return redirect()->route('list-post')->with('message', 'Create post success');
    }

    public function update($id, Request $request)
    {
        $post = Post::find($id);
        if ($post) {
            $user = auth()->user();
            $data_view = [
                'content' => 'post/edit',
                'post' => $post,
                'user' => $user,
            ];
            return view('mainLayout', $data_view);
        } else {
            return redirect()->route('list-post')->with('message', 'Create post success');
        }
    }

    public function do_update($id, Request $request)
    {
        $post = Post::find($id);
        $user = auth()->user();
        if ($post && ($user->hasRole("admin") || $user->id === $post->created_by)) {
            if ($user->hasRole("admin")) {
                $post->publish = $request->publish;
                $post->publish_at = $request->publish_at;
            } else{
                $post->post_name = $request->post_name;
                $post->post_description = $request->post_description;
            }
            $post->save();
            return redirect()->route('list-post')->with('message', 'Update post success');
        } else {
            return redirect()->route('list-post');
        }
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $user = auth()->user();
        if ($post && ($user->hasRole("admin") || $user->id === $post->created_by)) {
            $post->active = 0;
            $post->save();
            return redirect()->route('list-post')->with('message', 'Delete post success');
        } else {
            return redirect()->route('list-post');
        }
    }
}
