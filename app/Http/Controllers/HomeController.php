<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request){
        $list = Post::publish()->orderByDesc("id")->paginate(10);
        $data_view = [
            'content' => 'home/index',
            'data' => $list,
        ];
        return view('mainLayout', $data_view);
    }
}
