<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    // public $token = true;

    public function login(Request $request)
    {
        if (!auth()->check()) {
            $data_view = [
                'content' => 'auth/login',
            ];
            return view('mainLayout', $data_view);
        } else {
            return redirect()->route('homepage');
        }
    }

    public function do_login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_email' => 'required|min:0|max:255',
                'password' => 'required|min:0|max:255',
            ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors(),
            ]);
        }
        $credentials = [
            'user_email' => $request->user_email,
            'password' => $request->password,
            'active' => 1,
        ];
        if (Auth::guard('web')->attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->route('homepage');
        } else {
            return redirect()->back()->with('errors', 'Tài khoản hoặc mật khẩu không đúng');
        }
    }

    public function register(Request $request)
    {
        if (!auth()->check()) {
            $data_view = [
                'content' => 'auth/register',
            ];
            return view('mainLayout', $data_view);
        } else {
            return redirect()->route('homepage');
        }
    }

    public function do_register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'full_name' => 'required|min:0|max:255',
                'user_email' => 'required|email|unique:users|min:0|max:255',
                'password' => 'required|min:0|max:255',
            ]);
        if ($validator->fails()) {
            return redirect()->back()->with("errors", $validator->errors());
        }

        $user = new User();
        $user->full_name = $request->full_name;
        $user->user_email = $request->user_email;
        $user->password = bcrypt($request->password);
        $user->save();
        $user->assignRole([2]);
        $credentials = [
            'user_email' => $request->user_email,
            'password' => $request->password,
            'active' => 1,
        ];
        if (Auth::guard('web')->attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->route('homepage');
        } else {
            return redirect()->back()->with('errors', 'Tài khoản hoặc mật khẩu không đúng');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('homepage');
    }


}
