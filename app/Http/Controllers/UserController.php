<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
class UserController extends Controller
{
    public function __construct()
    {
        $this->user_model = new User();
    }

    public function get_list_users(Request $request){
        $list = $this->user_model->get_list_users($request);
        $query = [
            'key_search' => $request->key_search,
            'role_name' => $request->role_name
        ];
        $list->appends($query);
        $all_roles = Role::get();
        $data_view = [
            'content' => 'user/userList',
            'data' => $list,
            'query' => $query,
            'admin' => auth()->user(),
            'all_roles' => $all_roles,
        ];
        return view('mainLayout', $data_view);
    }

    public function create_user()
    {
        $roles = Role::get();
        $data_view = [
            'content' => 'user/userEdit',
            'roles' => $roles,
        ];
        return view('mainLayout', $data_view);
    }

    public function do_create_user(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_name' => 'required',
                'user_email' => 'required|unique:users,user_email',
                'password' => 'required|same:confirm-password',
                'role_id' => 'required'
            ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }
        $user = User::create([
            'user_email' => $request->user_email,
            'user_name' => $request->user_name,
            'user_phone' => $request->user_phone,
            'password' => bcrypt($request->password),
            'created_by' => auth()->user()->id,
        ]);
        $user->assignRole([$request->role_id]);

        return redirect()->route('list-user')
                        ->with('message','User created successfully');
    }


    public function edit_user($id)
    {
        $user = User::find($id);
        if(!empty($user)){
            $roles = Role::get();
            $role_ids = $user
                ->roles
                ->pluck('id')
                ->toArray();
            $user['role_id'] = !empty($role_ids[0]) ? $role_ids[0] : '';
            $data_view = [
                'content' => 'user/userEdit',
                'user' => $user,
                'roles' => $roles,
            ];
            return view('mainLayout', $data_view);
        } else {
            return redirect()->route('list-user')->with('errors', 'User not found');
        }
    }

    public function do_edit_user($id, Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_name' => 'required',
                'role_id' => 'required'
            ]);;
        if ($validator->fails()) {
            redirect()->back()->with('errors', $validator->errors());
        }

        $user = User::find($id);
        $user->update([
            'user_name' => $request->user_name,
            'user_phone' => $request->user_phone,
            'updated_by' => auth()->user()->id,
        ]);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole([$request->role_id]);
        return redirect()->route('list-user')->with('message', 'Update user success');
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|numeric|min:1',
            ]);;
        if ($validator->fails()) {
            redirect()->back()->with('errors', $validator->errors());
        }
        $id = $request->id;
        $user = User::find($id);
        if($user){
            $user->active = 0;
            $user->updated_by = auth()->user()->id;
            $user->save();
        }
        return redirect()->route('list-user')->with('message', 'Delete user success');
    }

    public function change_password()
    {
        $data_view = [
            'content' => 'user/changePassword',
        ];
        return view('mainLayout', $data_view);
    }

    public function do_change_password(Request $request){
        $validator = Validator::make($request->all(),
            [
                'old_password' => 'required',
                'password' => 'required|same:confirm-password',
            ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }
        $user = auth()->user();
        if(Hash::check($request->old_password, $user->password)){
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect()->back()->with('message', 'Update password success');
        } else{
            return redirect()->back()->with('errors', 'Old password not right');
        }
    }
}
